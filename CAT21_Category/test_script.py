import pyModeS as pms

msg = '8d8007f7e9481200093810fb7f71'
b = pms.hex2bin(msg)

while True:
	try:
		print("Downlink Format:", b[0:5], pms.bin2int(b[0:5]),pms.adsb.df(msg))
		print("Capability:", b[5:8], pms.bin2int(b[5:8]))
		print("ICAO Address:", b[8:32], pms.adsb.icao(msg))
	    print("Type Code:", b[32:37], pms.bin2int(b[32:37]), pms.adsb.typecode(msg))
	    print("Surveillance Status: ", b[37:39], pms.bin2int(b[37:39]))
	    print("NIC Supplement - B: ",b[40], pms.adsb.nic_b(msg))
	    print("Altitude(ft): ", pms.adsb.altitude(msg))
	    print("Time: ", pms.bin2int(b[53]))
	    print("CPR Even(0)/Odd(1) Frame Flag:", b[54], pms.bin2int(b[54]))
	    print("Latitude:", b[54:71], pms.bin2int(b[54:71]))
	    print("Longitude:", b[71:88], pms.bin2int(b[71:88]))
	    print("Parity Generator ID:", b[88:112], pms.bin2int(b[88:112]))
	    print("Mach Number: " ,pms.commb.mach60(msg))     # Mach number (-)
	    print("Downlink Format:", b[0:5], pms.adsb.df(msg))
	    print("Capability:", b[5:8], pms.bin2int(b[5:8]))
	    print("ICAO Address:", b[8:32], pms.adsb.icao(msg))
	    print("Type Code:", b[32:37], pms.bin2int(b[32:37]), pms.adsb.typecode(msg))
	    print("Sub-Type Code:", b[37:40], pms.bin2int(b[37:40]))
	    print("Intent Change Flag:", b[41], pms.bin2int(b[41]))
	    print("Reserved-A: ", b[42], pms.bin2int(b[42]))
	    print("Velocity Uncertainity(NAC): ", b[42:45], pms.bin2int(b[42:45]), pms.adsb.nac_v(msg))
	    print("East-West Velocity Sign: ", b[46], pms.bin2int(b[46]))
	    print("East-West Velocity: ", b[46:56], pms.bin2int(b[46:56]))
	    print("North-South Velocity Sign: ", b[57], pms.bin2int(b[57]))
	    print("North-South Velocity: ", b[57:67], pms.bin2int(b[57:67]))
	    print("Vertical Rate Source: ", b[68], pms.bin2int(b[68]))
	    print("Vertical Rate Sign: ", b[69], pms.bin2int(b[69]))
	    print("Vertical Rate: ", b[69:78], pms.bin2int(b[69:78]))
	    print("Reserved-B: ", b[78:80], pms.bin2int(b[78:80]))
	    print("Diff. from baro alt, sign: ", b[81], pms.bin2int(b[81]))
	    print("Diff from baro alt: ", b[81:88], pms.bin2int(b[81:88]))
	    print("Mach Number: ",pms.commb.mach60(msg))     # Mach number (-)
	    print("Barometric ressure: ", pms.commb.p40baro(msg))
	    print("True airspeed (kt): ", pms.commb.tas50(msg))
	    print("Mach number: ", pms.commb.mach60(msg))
	    print("Wind speed (kt) and direction (true) (deg): ", pms.commb.wind44(msg))
	    print("Static air temperature(): ", pms.commb.temp44(msg))
	    print("Average static pressure (hPa): ", pms.commb.p44(msg))
	    print("Humidity (%): ", pms.commb.hum44(msg))     
	    print("FMS selected altitude (ft): ", pms.commb.selalt40fms(msg))
	    print("Radio height (ft): ", pms.commb.rh45(msg))
	    break
	except Exception:
    	pass
