#!/usr/bin/python
 
###########################################################
#
#
# Written by : Faheem Ashraf & Aman Rawat
# Mail : info@botlabdynamics.com
# Created date: May 01, 2020
# Last modified: Aug 08, 2020 
# Copyrights : BotLab Dynamics Pvt Ltd.
# Tested with :  Python 3.7
#
##########################################################

#Import Libraries

import socket
import redis
redis_host = "localhost"
redis_port = 6379
import time
import struct
from sic_sac_sid_sms import cat23Data
from gpsData import GpsData
import pyModeS as pms
from datetime import datetime
from pyModeS import common, adsb, commb, bds
import asyncio
import websockets
import time
odd ='Odd'
even='Even'
count = 0
x=[0]*15

#Read incoming raw data from sockets

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('192.168.1.1', 10003))
mreq = struct.pack("=4sl", socket.inet_aton("232.1.1.11"), socket.INADDR_ANY)

def data():
	global count,alt
	infoGps=GpsData()
	date_time =f'{infoGps[1]} {infoGps[3]} (UTC)'
	m=s.recv(10240)
	msg = m[9:].hex()
	cat23=cat23Data()
	cat23=cat23.replace(",", ":")
	cat23=list(cat23.split(":")) 
	unix_epoch =time.time()

	if msg[0] == "8" or msg[0] == "a":
			df = common.df(msg)
			icao = common.icao(msg)
			message_bin = pms.hex2bin(msg)
			#Classify into various DownLink Formats
			if df == 17 or df == 18:
				tc = common.typecode(msg)
				#Classify into various TypeCodes
				if 1 <= tc <= 4:  # callsign
					
					callsign = adsb.callsign(msg)
					ec=pms.adsb.category(msg)
					abc = {'A0' : '-72 dBm','A1' : '-79 dBm','A2' : '-79 dBm','A3' : '-87 dBm'}
					amp = abc['A'+str(ec)]
					df17_1_4=f'''Timestamp: {unix_epoch} {date_time}<br><br>Item: I021/080 (Target Address)<br>&emsp;&emsp;&emsp;&emsp; Addr (Target Address): {icao}<br><br>
					Item: I021/170 (Target Identification)<br>&emsp;&emsp;&emsp;&emsp; TId: {callsign}<br><br>
					Item: I021/010 (Data Source Identification)<br>&emsp;&emsp;&emsp;&emsp; SAC (System Area Code): {cat23[21]}<br>&emsp;&emsp;&emsp;&emsp; SIC (System Identification Code): {cat23[23]}<br><br>
						Item: I021/015 (Service Identification)<br>&emsp;&emsp;&emsp;&emsp; SID (Service Identification): {cat23[31]}<br>&emsp;&emsp;&emsp;&emsp;  STYP (Type of Service): {cat23[33]}<br><br>
						Item: I021/016 (Service Management)<br>&emsp;&emsp;&emsp;&emsp; RP (Report Period for Category 021 Reports) : {cat23[41]}<br>&emsp;&emsp;&emsp;&emsp; SC (Service Class) : {cat23[43]}<br>&emsp;&emsp;&emsp;&emsp;
						 spare (Spare bit set to zero) : {cat23[45]}<br>&emsp;&emsp;&emsp;&emsp; FX (FX)= {cat23[47]}<br>&emsp;&emsp;&emsp;&emsp; SSRP (Service Status Reporting Period)= {cat23[49]}<br><br>
						 Item: I021/020 (Emitter Category)<br>&emsp;&emsp;&emsp;&emsp; ECAT (Emitter Category): {ec}<br><br>Item: I132 (Signal Amplitude)<br>&emsp;&emsp;&emsp;&emsp; SAM (Message Amplitude): {amp}<br><br>
						 Item: I021/295 (Data Ages)<br>&emsp;&emsp;&emsp;&emsp; DA(Data Age): {count}'''
					x[0]=df17_1_4

				elif 5 <= tc <= 8:  # surface position
					cord = pms.adsb.airborne_position_with_ref(msg, float(infoGps[6]) , float(infoGps[8]))
					lat, lon = cord[0], cord[1]
					df17_5_8=f'''Timestamp: {unix_epoch} {date_time}<br><br>Item: I021/080 (Target Address)<br>&emsp;&emsp;&emsp;&emsp; Addr (Target Address): {icao}<br><br>
					Item: I021/010 (Data Source Identification)<br>&emsp;&emsp;&emsp;&emsp; SAC (System Area Code): {cat23[21]}<br>&emsp;&emsp;&emsp;&emsp; SIC (System Identification Code): {cat23[23]}<br><br>
						Item: I021/015 (Service Identification)<br>&emsp;&emsp;&emsp;&emsp; SID (Service Identification): {cat23[31]}<br>&emsp;&emsp;&emsp;&emsp;  STYP (Type of Service): {cat23[33]}<br><br>
						Item: I021/016 (Service Management)<br>&emsp;&emsp;&emsp;&emsp; RP (Report Period for Category 021 Reports) : {cat23[41]}<br>&emsp;&emsp;&emsp;&emsp; SC (Service Class) : {cat23[43]}<br>&emsp;&emsp;&emsp;&emsp;
						spare (Spare bit set to zero) : {cat23[45]}<br>&emsp;&emsp;&emsp;&emsp; FX (FX)= {cat23[47]}<br>&emsp;&emsp;&emsp;&emsp; SSRP (Service Status Reporting Period)= {cat23[49]}<br><br>
						Item: I021/110 (Trajectory Intent)<br>&emsp;&emsp;&emsp;&emsp; Lat (Latitude in WGS.84): {lat}<br>&emsp;&emsp;&emsp;&emsp; Lon (Longitude in WGS.84): {lon}<br><br>
						Item: I021/130 (Position in WGS-84 Co-ordinates)<br>&emsp;&emsp;&emsp;&emsp; Lat (Latitude in WGS.84 in twos complement. Range -90 <= latitude <= 90 deg.): {lat}<br>&emsp;&emsp;&emsp;&emsp;
						Lon (Longitude in WGS.84 in twos complement. Range -180 <= longitude < 180 deg.): {lon}<br><br>
						Item: I021/295 (Data Ages)<br>&emsp;&emsp;&emsp;&emsp; DA(Data Age): {count}'''
					x[1]=df17_5_8

				elif 9 <= tc <= 18:
				   
					alt = adsb.altitude(msg)
					cord = pms.adsb.airborne_position_with_ref(msg, float(infoGps[6]), float(infoGps[8]))
					lat, lon = cord[0], cord[1]
					df17_9_18=f'''Timestamp: {unix_epoch} {date_time}<br><br>Item: I021/080 (Target Address)<br>&emsp;&emsp;&emsp;&emsp; Addr (Target Address): {icao}<br><br>
					Item: I021/010 (Data Source Identification)<br>&emsp;&emsp;&emsp;&emsp; SAC (System Area Code): {cat23[21]}<br>&emsp;&emsp;&emsp;&emsp; SIC (System Identification Code): {cat23[23]}<br><br>
						Item: I021/015 (Service Identification)<br>&emsp;&emsp;&emsp;&emsp; SID (Service Identification): {cat23[31]}<br>&emsp;&emsp;&emsp;&emsp;  STYP (Type of Service): {cat23[33]}<br><br>
						Item: I021/016 (Service Management)<br>&emsp;&emsp;&emsp;&emsp; RP (Report Period for Category 021 Reports) : {cat23[41]}<br>&emsp;&emsp;&emsp;&emsp; SC (Service Class) : {cat23[43]}<br>&emsp;&emsp;&emsp;&emsp;
						 spare (Spare bit set to zero) : {cat23[45]}<br>&emsp;&emsp;&emsp;&emsp; FX (FX)= {cat23[47]}<br>&emsp;&emsp;&emsp;&emsp; SSRP (Service Status Reporting Period)= {cat23[49]}<br><br>
						 Item: I021/040 (Target Report Descriptor)<br>&emsp;&emsp;&emsp;&emsp; DCR (Differential Correction): 0 No differential correction (ADS-B)<br>&emsp;&emsp;&emsp;&emsp;
						 GBS (Ground Bit Setting): 0 Ground Bit not set<br>&emsp;&emsp;&emsp;&emsp; SIM (Simulated Target): 0 Actual target report<br>&emsp;&emsp;&emsp;&emsp; TST (Test Target): 0 Default<br>&emsp;&emsp;&emsp;&emsp;
						 RAB (Report description ): 0 Report from target transponder<br>&emsp;&emsp;&emsp;&emsp; SAA (Selected Altitude Available): 0 Equipment capable to provide Selected Altitude<br>&emsp;&emsp;&emsp;&emsp;
						 SPI (Special Position Identification): 0 Absence of SPI<br>&emsp;&emsp;&emsp;&emsp; spare (Spare bits set to 0): 0<br>&emsp;&emsp;&emsp;&emsp; ATP (Address Type): 1 24-Bit ICAO address<br>&emsp;&emsp;&emsp;&emsp;
						 ARC (Altitude Reporting Capability): 1 25 ft<br><br>Item: I021/I110 (Trajectory Intent)<br>&emsp;&emsp;&emsp;&emsp; Lat (Latitude in WGS.84): {lat}<br>&emsp;&emsp;&emsp;&emsp; Lon (Longitude in WGS.84): {lon}<br>&emsp;&emsp;&emsp;&emsp; 
						 Alt (Altitude): {alt}<br><br>Item: I021/130 (Position in WGS-84 Co-ordinates)<br>&emsp;&emsp;&emsp;&emsp; Lat (Latitude in WGS.84 in twos complement. Range -90 <= latitude <= 90 deg.): {lat}<br>&emsp;&emsp;&emsp;&emsp;
						 Lon (Longitude in WGS.84 in twos complement. Range -180 <= longitude < 180 deg.): {lon}<br><br>
						 Item: I021/295 (Data Ages)<br>&emsp;&emsp;&emsp;&emsp; DA(Data Age): {count}'''
					x[2]=df17_9_18


				elif tc == 19:
					
					nuc_v = tuple(map(str,pms.adsb.nuc_v(msg)))
					nuc_v='; '.join(map(str, nuc_v))
					nac_v = pms.adsb.nac_v(msg)
					df17_19=f'''Timestamp: {unix_epoch} {date_time}<br><br>Item: I021/080 (Target Address)<br>&emsp;&emsp;&emsp;&emsp; Addr (Target Address): {icao}<br><br>
					Item: I021/010 (Data Source Identification)<br>&emsp;&emsp;&emsp;&emsp; SAC (System Area Code): {cat23[21]}<br>&emsp;&emsp;&emsp;&emsp; SIC (System Identification Code): {cat23[23]}<br><br>
						Item: I021/015 (Service Identification)<br>&emsp;&emsp;&emsp;&emsp; SID (Service Identification): {cat23[31]}<br>&emsp;&emsp;&emsp;&emsp;  STYP (Type of Service): {cat23[33]}<br><br>
						Item: I021/016 (Service Management)<br>&emsp;&emsp;&emsp;&emsp; RP (Report Period for Category 021 Reports) : {cat23[41]}<br>&emsp;&emsp;&emsp;&emsp; SC (Service Class) : {cat23[43]}<br>&emsp;&emsp;&emsp;&emsp;
						 spare (Spare bit set to zero) : {cat23[45]}<br>&emsp;&emsp;&emsp;&emsp; FX (FX)= {cat23[47]}<br>&emsp;&emsp;&emsp;&emsp; SSRP (Service Status Reporting Period)= {cat23[49]}<br><br>
						Item: I021/090 (Quality Indicators)<br>&emsp;&emsp;&emsp;&emsp; NACv (Navigation Accuracy Category For Velocity): {nac_v}<br>&emsp;&emsp;&emsp;&emsp; NUCv (Navigation Uncertainty Category For Velocity): {nuc_v}<br><br>
						Item: I021/295 (Data Ages)<br>&emsp;&emsp;&emsp;&emsp; DA(Data Age): {count}'''
					x[3]=df17_19
					

				elif 20 <= tc <= 22:  # airborne position
					alt = adsb.altitude(msg)
					cord = pms.adsb.airborne_position_with_ref(msg, float(infoGps[6]), float(infoGps[8]))
					lat, lon = cord[0], cord[1]
					df17_20_22=f'''Timestamp: {unix_epoch} {date_time}<br><br>Item: I021/080 (Target Address)<br>&emsp;&emsp;&emsp;&emsp; Addr (Target Address): {icao}<br><br>
					Item: I021/010 (Data Source Identification)<br>&emsp;&emsp;&emsp;&emsp; SAC (System Area Code): {cat23[21]}<br>&emsp;&emsp;&emsp;&emsp; SIC (System Identification Code): {cat23[23]}<br><br>
						Item: I021/015 (Service Identification)<br>&emsp;&emsp;&emsp;&emsp; SID (Service Identification): {cat23[31]}<br>&emsp;&emsp;&emsp;&emsp;  STYP (Type of Service): {cat23[33]}<br><br>
						Item: I021/016 (Service Management)<br>&emsp;&emsp;&emsp;&emsp; RP (Report Period for Category 021 Reports) : {cat23[41]}<br>&emsp;&emsp;&emsp;&emsp; SC (Service Class) : {cat23[43]}<br>&emsp;&emsp;&emsp;&emsp;
						 spare (Spare bit set to zero) : {cat23[45]}<br>&emsp;&emsp;&emsp;&emsp; FX (FX)= {cat23[47]}<br>&emsp;&emsp;&emsp;&emsp; SSRP (Service Status Reporting Period)= {cat23[49]}<br><br>
						Item: I021/040 (Target Report Descriptor)<br>&emsp;&emsp;&emsp;&emsp; DCR (Differential Correction): 0 No differential correction (ADS-B)<br>&emsp;&emsp;&emsp;&emsp;
						 GBS (Ground Bit Setting): 0 Ground Bit not set<br>&emsp;&emsp;&emsp;&emsp; SIM (Simulated Target): 0 Actual target report<br>&emsp;&emsp;&emsp;&emsp; TST (Test Target): 0 Default<br>&emsp;&emsp;&emsp;&emsp;
						 RAB (Report description ): 0 Report from target transponder<br>&emsp;&emsp;&emsp;&emsp; SAA (Selected Altitude Available): 0 Equipment capable to provide Selected Altitude<br>&emsp;&emsp;&emsp;&emsp;
						 SPI (Special Position Identification): 0 Absence of SPI<br>&emsp;&emsp;&emsp;&emsp; spare (Spare bits set to 0): 0<br>&emsp;&emsp;&emsp;&emsp; ATP (Address Type): 1 24-Bit ICAO address<br>&emsp;&emsp;&emsp;&emsp;
						 ARC (Altitude Reporting Capability): 1 25 ft<br><br>Item: I021/I110 (Trajectory Intent)<br>&emsp;&emsp;&emsp;&emsp; Lat (Latitude in WGS.84): {lat}<br>&emsp;&emsp;&emsp;&emsp; Lon (Longitude in WGS.84): {lon}<br>&emsp;&emsp;&emsp;&emsp; 
						 Alt (Altitude): {alt}<br><br>Item: I021/130 (Position in WGS-84 Co-ordinates)<br>&emsp;&emsp;&emsp;&emsp; Lat (Latitude in WGS.84 in twos complement. Range -90 <= latitude <= 90 deg.): {lat}<br>&emsp;&emsp;&emsp;&emsp;
						 Lon (Longitude in WGS.84 in twos complement. Range -180 <= longitude < 180 deg.): {lon}<br><br>
						 Item: I140 (Geometric Height)<br>&emsp;&emsp;&emsp;&emsp; GAlt (Geometric Height): {alt}<br><br>
						 Item: I021/295 (Data Ages)<br>&emsp;&emsp;&emsp;&emsp; DA(Data Age): {count}'''
					x[4]=df17_20_22

				elif tc == 28:
					sub_type = pms.bin2int(message_bin[37:40])
					if sub_type == 1:
						priority_status = pms.bin2int(message_bin[40:43]) #Priority Status
						df17_28_1=f'''Timestamp: {unix_epoch} {date_time}<br><br>Item: I021/080 (Target Address)<br>&emsp;&emsp;&emsp;&emsp; Addr (Target Address): {icao}<br><br>
					Item: I021/010 (Data Source Identification)<br>&emsp;&emsp;&emsp;&emsp; SAC (System Area Code): {cat23[21]}<br>&emsp;&emsp;&emsp;&emsp; SIC (System Identification Code): {cat23[23]}<br><br>
						Item: I021/015 (Service Identification)<br>&emsp;&emsp;&emsp;&emsp; SID (Service Identification): {cat23[31]}<br>&emsp;&emsp;&emsp;&emsp;  STYP (Type of Service): {cat23[33]}<br><br>
						Item: I021/016 (Service Management)<br>&emsp;&emsp;&emsp;&emsp; RP (Report Period for Category 021 Reports) : {cat23[41]}<br>&emsp;&emsp;&emsp;&emsp; SC (Service Class) : {cat23[43]}<br>&emsp;&emsp;&emsp;&emsp;
						 spare (Spare bit set to zero) : {cat23[45]}<br>&emsp;&emsp;&emsp;&emsp; FX (FX)= {cat23[47]}<br>&emsp;&emsp;&emsp;&emsp; SSRP (Service Status Reporting Period)= {cat23[49]}<br><br>
						 Item: I021/200 (Target Status)<br>&emsp;&emsp;&emsp;&emsp; PS (Priority Status): {priority_status}<br><br>
						 Item: I021/295 (Data Ages)<br>&emsp;&emsp;&emsp;&emsp; DA(Data Age): {count}'''
						x[5]=df17_28_1

					elif sub_type == 2:
						ara = pms.bin2int(message_bin[40:54]) #Active Resolution Advisories(ARA)
						rac_records = pms.bin2int(message_bin[54:58]) #RACs Record
						rat = pms.bin2int(message_bin[58]) #RA Terminated
						mte = pms.bin2int(message_bin[59]) #Multiple Threat Encounter
						tti = pms.bin2int(message_bin[60:62]) #Threat Type Indicator
						tid = pms.bin2int(message_bin[62:88]) #Threat Identity Data
						df17_28_2=f'''Timestamp: {unix_epoch} {date_time}<br><br>Item: I021/080 (Target Address)<br>&emsp;&emsp;&emsp;&emsp; Addr (Target Address): {icao}<br><br>
					Item: I021/010 (Data Source Identification)<br>&emsp;&emsp;&emsp;&emsp; SAC (System Area Code): {cat23[21]}<br>&emsp;&emsp;&emsp;&emsp; SIC (System Identification Code): {cat23[23]}<br><br>
						Item: I021/015 (Service Identification)<br>&emsp;&emsp;&emsp;&emsp; SID (Service Identification): {cat23[31]}<br>&emsp;&emsp;&emsp;&emsp;  STYP (Type of Service): {cat23[33]}<br><br>
						Item: I021/016 (Service Management)<br>&emsp;&emsp;&emsp;&emsp; RP (Report Period for Category 021 Reports) : {cat23[41]}<br>&emsp;&emsp;&emsp;&emsp; SC (Service Class) : {cat23[43]}<br>&emsp;&emsp;&emsp;&emsp;
						spare (Spare bit set to zero) : {cat23[45]}<br>&emsp;&emsp;&emsp;&emsp; FX (FX)= {cat23[47]}<br>&emsp;&emsp;&emsp;&emsp; SSRP (Service Status Reporting Period)= {cat23[49]}<br><br>
						Item: I021/260 (ACAS Resolution Advisory Report)<br>&emsp;&emsp;&emsp;&emsp; ARA (Active Resolution Advisories): {ara}<br>&emsp;&emsp;&emsp;&emsp; RAC (RACs Records): {rac_records}<br>&emsp;&emsp;&emsp;&emsp;
    					RA (RA Terminated): {rat}<br>&emsp;&emsp;&emsp;&emsp; MTE (Multiple Threat Encounter): {mte}<br>&emsp;&emsp;&emsp;&emsp; TTI (Threat Type Indicator): {tti}<br>&emsp;&emsp;&emsp;&emsp; TTD (Threat Identity Data): {tid}<br><br>
						Item: I021/295 (Data Ages)<br>&emsp;&emsp;&emsp;&emsp; DA(Data Age): {count}'''
						x[6]=df17_28_2

				elif tc == 29:
					
					nic_baro = pms.bin2int(message_bin[75]) #Navigation Integrity Category_Baro Level(NIC BARO)
					sil =tuple(map(str,pms.adsb.sil(msg,2))) 
					sil='; '.join(map(str,sil))
					nac_p =tuple(map(str,pms.adsb.nac_p(msg)))#Navigation Accuracy Category - Position
					nac_p='; '.join(map(str, nac_p))  
					df17_29=f'''Timestamp: {unix_epoch} {date_time}<br><br>Item: I021/080 (Target Address)<br>&emsp;&emsp;&emsp;&emsp; Addr (Target Address): {icao}<br><br>
					Item: I021/010 (Data Source Identification)<br>&emsp;&emsp;&emsp;&emsp; SAC (System Area Code): {cat23[21]}<br>&emsp;&emsp;&emsp;&emsp; SIC (System Identification Code): {cat23[23]}<br><br>
						Item: I021/015 (Service Identification)<br>&emsp;&emsp;&emsp;&emsp; SID (Service Identification): {cat23[31]}<br>&emsp;&emsp;&emsp;&emsp;  STYP (Type of Service): {cat23[33]}<br><br>
						Item: I021/016 (Service Management)<br>&emsp;&emsp;&emsp;&emsp; RP (Report Period for Category 021 Reports) : {cat23[41]}<br>&emsp;&emsp;&emsp;&emsp; SC (Service Class) : {cat23[43]}<br>&emsp;&emsp;&emsp;&emsp;
						 spare (Spare bit set to zero) : {cat23[45]}<br>&emsp;&emsp;&emsp;&emsp; FX (FX)= {cat23[47]}<br>&emsp;&emsp;&emsp;&emsp; SSRP (Service Status Reporting Period)= {cat23[49]}<br><br>
						 Item: I021/295 (Data Ages)<br>&emsp;&emsp;&emsp;&emsp; DA(Data Age): {count}'''
					x[7]=df17_29

				elif tc == 31:
					sub_type = pms.bin2int(message_bin[37:40])
					if sub_type == 0:
						tcas_ops = pms.bin2int(message_bin[42]) #TCAS Operational: 
						arv_cap = pms.bin2int(message_bin[46]) #Air-Referenced Velocity Report(ARV) Report Capability: 
						ts = pms.bin2int(message_bin[47]) #Target State Report Capability(TS): 
						tc = pms.bin2int(message_bin[48:50]) #Target Change Report Capability(TC): 
						nac_v = pms.bin2int(message_bin[48:51]) #NACv:  
						icas_ad = pms.bin2int(message_bin[58]) #TCAS Report Advisory Active: 
						indent_sw = pms.bin2int(message_bin[59]) #INDENT Switch Active: 
						rec_atc = pms.bin2int(message_bin[60]) 	#Reserved for Recieving ATC Services: 
						single_antenna_flag = pms.bin2int(message_bin[61]) #Single Antenna Flag: 
						adsb_v = pms.bin2int(message_bin[72:75]) #MOPS Version: 
						gva = pms.bin2int(message_bin[80:82]) #Geometric Vertical Accuracy (GVA): 
						nic_baro = pms.bin2int(message_bin[84]) #NICbaro :
						sil_s = pms.bin2int(message_bin[86]) #SIL Suppl :
						nac_p = pms.adsb.nac_p(msg)
						nic_p = pms.adsb.nic_a_c(msg) #NIC Position
						sil =tuple(map(str,pms.adsb.sil(msg,adsb_v)))#Source Integity Level(SIL) 
						sil='; '.join(map(str,sil))
						df17_31_0=f'''Timestamp: {unix_epoch} {date_time}<br><br>Item: I021/080 (Target Address)<br>&emsp;&emsp;&emsp;&emsp; Addr (Target Address): {icao}<br><br>
					Item: I021/010 (Data Source Identification)<br>&emsp;&emsp;&emsp;&emsp; SAC (System Area Code): {cat23[21]}<br>&emsp;&emsp;&emsp;&emsp; SIC (System Identification Code): {cat23[23]}<br><br>
						Item: I021/015 (Service Identification)<br>&emsp;&emsp;&emsp;&emsp; SID (Service Identification): {cat23[31]}<br>&emsp;&emsp;&emsp;&emsp;  STYP (Type of Service): {cat23[33]}<br><br>
						Item: I021/016 (Service Management)<br>&emsp;&emsp;&emsp;&emsp; RP (Report Period for Category 021 Reports) : {cat23[41]}<br>&emsp;&emsp;&emsp;&emsp; SC (Service Class) : {cat23[43]}<br>&emsp;&emsp;&emsp;&emsp;
						 spare (Spare bit set to zero) : {cat23[45]}<br>&emsp;&emsp;&emsp;&emsp; FX (FX)= {cat23[47]}<br>&emsp;&emsp;&emsp;&emsp; SSRP (Service Status Reporting Period)= {cat23[49]}<br><br>
						Item: I021/210 (MOPS Version)<br>&emsp;&emsp;&emsp;&emsp;MOPS: {adsb_v}<br><br>
						Item: I021/008 (Aircraft Operational Status)<br>&emsp;&emsp;&emsp;&emsp; TCASRA (TCAS Resolution Advisory active): {icas_ad}<br>&emsp;&emsp;&emsp;&emsp;
						 TCR (Target Change Report Capability): {tc}<br>&emsp;&emsp;&emsp;&emsp; TSR (Target State Report Capability): {ts}<br>&emsp;&emsp;&emsp;&emsp;
        				ARV (Air-Referenced Velocity Report Capability): {arv_cap}<br>&emsp;&emsp;&emsp;&emsp; CDTI (Cockpit Display of Traffic Information airborne): 0<br>&emsp;&emsp;&emsp;&emsp;
       					TCASST (TCAS System Status): {tcas_ops}<br>&emsp;&emsp;&emsp;&emsp; SA (Single Antenna): {single_antenna_flag}<br><br>Item: I021/090 (Quality Indicators)<br>&emsp;&emsp;&emsp;&emsp;
        				 NACp (Navigation Accuracy Category For Position): {nac_p}<br>&emsp;&emsp;&emsp;&emsp; NIC (Navigation Integrity Category For Position): {nic_p}<br>&emsp;&emsp;&emsp;&emsp; SIL (Surveillance integrity level): {sil}<br>&emsp;&emsp;&emsp;&emsp;
						  NACv (Navigation Accuracy Category For Velocity): {nac_v}<br>&emsp;&emsp;&emsp;&emsp; GVA (Geometric Altitude Accuracy): {gva}<br>&emsp;&emsp;&emsp;&emsp;
						   NICbaro (Navigation Integrity Category for Barometric Altitude): {nic_baro}<br>&emsp;&emsp;&emsp;&emsp; SILS (SIL-Supplement): {sil_s}<br><br>
						   Item: I021/295 (Data Ages)<br>&emsp;&emsp;&emsp;&emsp; DA(Data Age): {count}'''
						x[8]=df17_31_0

					elif sub_type == 1:
						
						poa = pms.bin2int(message_bin[42]) #Position Offset Applied(POA): 
						b2_low = pms.bin2int(message_bin[46]) # b2 Low  
						l_w = pms.bin2int(message_bin[52:56]) #Length/Width of the Aircraft:
						indent_sw = pms.bin2int(message_bin[59]) #INDENT Switch Active: 
						rec_atc = pms.bin2int(message_bin[60]) #Reserved for Recieving ATC Services:  
						single_antenna_flag = pms.bin2int(message_bin[61]) #Single Antenna Flag:  
						adsb_v = pms.bin2int(message_bin[72:75]) #MOPS Version: 
						nac_p = pms.adsb.nac_p(msg)
						df17_31_1=f'''Timestamp: {unix_epoch} {date_time}<br><br>Item: I021/080 (Target Address)<br>&emsp;&emsp;&emsp;&emsp; Addr (Target Address): {icao}<br><br>
					Item: I021/010 (Data Source Identification)<br>&emsp;&emsp;&emsp;&emsp; SAC (System Area Code): {cat23[21]}<br>&emsp;&emsp;&emsp;&emsp; SIC (System Identification Code): {cat23[23]}<br><br>
						Item: I021/015 (Service Identification)<br>&emsp;&emsp;&emsp;&emsp; SID (Service Identification): {cat23[31]}<br>&emsp;&emsp;&emsp;&emsp;  STYP (Type of Service): {cat23[33]}<br><br>
						Item: I021/016 (Service Management)<br>&emsp;&emsp;&emsp;&emsp; RP (Report Period for Category 021 Reports) : {cat23[41]}<br>&emsp;&emsp;&emsp;&emsp; SC (Service Class) : {cat23[43]}<br>&emsp;&emsp;&emsp;&emsp;
						spare (Spare bit set to zero) : {cat23[45]}<br>&emsp;&emsp;&emsp;&emsp; FX (FX)= {cat23[47]}<br>&emsp;&emsp;&emsp;&emsp; SSRP (Service Status Reporting Period)= {cat23[49]}<br><br>
						Item: I021/210 (MOPS Version)<br>&emsp;&emsp;&emsp;&emsp;MOPS: {adsb_v}<br><br>Item: I021/271 (Surface Capabilities and Characteristics)<br>&emsp;&emsp;&emsp;&emsp; POA (Position Offset Applied): {poa}<br>&emsp;&emsp;&emsp;&emsp;
						CDTI/S (Cockpit Display of Traffic Information Surface): 0<br>&emsp;&emsp;&emsp;&emsp; B2low (Class B2 transmit power < 70 Watts): {b2_low}<br>&emsp;&emsp;&emsp;&emsp;
						RAS (Receiving ATC Services): {rec_atc}<br>&emsp;&emsp;&emsp;&emsp; INDENT (Setting of INDENT Switch)): {indent_sw}<br>&emsp;&emsp;&emsp;&emsp; LWA (Length and Width of Aircraft): {l_w}<br><br>
						Item: I021/295 (Data Ages)<br>&emsp;&emsp;&emsp;&emsp; DA(Data Age): {count}'''
						x[9]=df17_31_1	
			elif df == 20:
				alt = str(common.altcode(msg))

			elif df == 21:
				sqw = common.idcode(msg)

			elif df == 20 or df == 21:
				labels = {
					"BDS10": "Data Link Capability",
					"BDS17": "GICB Capability",
					"BDS20": "Aircraft Identification",
					"BDS30": "ACAS Resolution",
					"BDS40": "Vertical Intention Report",
					"BDS50": "Track and Turn Report",
					"BDS60": "Heading and Speed Report",
					"BDS44": "Meteorological Routine Air Report",
					"BDS45": "Meteorological Hazard Report",
					"EMPTY": "[No information available]",
				}

				BDS = bds.infer(msg, mrar=True)

				if BDS == "BDS20":
					callsign = commb.cs20(msg)
					df2x_20=f'''Timestamp: {unix_epoch} {date_time}<br><br>Item: I021/080 (Target Address)<br>&emsp;&emsp;&emsp;&emsp; Addr (Target Address): {icao}<br><br>Item: I021/250 (Mode S MB Data)<br>&emsp;&emsp;&emsp;&emsp; MB (MB Data): {pms.data(msg)}<br><br>
					Item: I021/070 (Mode 3/A Code in Octal Representation)<br>&emsp;&emsp;&emsp;&emsp; V (Validated): 1 Code not validated<br>&emsp;&emsp;&emsp;&emsp;
					 G (Garbled): 0 Default<br>&emsp;&emsp;&emsp;&emsp; L (Last update): 0 Mode-3/A code derived during last update<br>&emsp;&emsp;&emsp;&emsp; spare (Spare bits set to 0): 0<br>&emsp;&emsp;&emsp;&emsp;
					 Mode3A (Mode-3/A reply in octal): {sqw}<br><br>Item: I021/295 (Data Ages)<br>&emsp;&emsp;&emsp;&emsp; DA(Data Age): {count}'''
					x[10]=df2x_20


				elif BDS == "BDS40":
					
					df2x_40=f'''Timestamp: {unix_epoch} {date_time}<br><br>Item: I021/080 (Target Address)<br>&emsp;&emsp;&emsp;&emsp; Addr (Target Address): {icao}<br><br>Item: I021/250 (Mode S MB Data)<br>&emsp;&emsp;&emsp;&emsp; MB (MB Data): {pms.data(msg)}<br><br>
					Item: I021/146 (Intermediate State Selected Altitude)<br>&emsp;&emsp;&emsp;&emsp; 
					ISSA (Intermediate State Selected Altitude): {commb.selalt40mcp(msg)}<br><br>Item: I021/I148 (Final State Selected Altitude)<br>&emsp;&emsp;&emsp;&emsp; FSSA (Final State Selected Altitude): {commb.selalt40fms(msg)}<br><br>
					Item: I021/295 (Data Ages)<br>&emsp;&emsp;&emsp;&emsp; DA(Data Age): {count}'''
					x[11]=df2x_40

				elif BDS == "BDS50":
					
					df2x_50=f'''Timestamp: {unix_epoch} {date_time}<br><br>Item: I021/080 (Target Address)<br>&emsp;&emsp;&emsp;&emsp; Addr (Target Address): {icao}<br><br>Item: I021/250 (Mode S MB Data)<br>&emsp;&emsp;&emsp;&emsp; MB (MB Data): {pms.data(msg)}<br><br>
					Item: I021/I151 (True Air Speed)<br>&emsp;&emsp;&emsp;&emsp; TAS (True Air Speed): {commb.tas50(msg)}<br><br>
					Item: I021/160 (Ground Vector)<br>&emsp;&emsp;&emsp;&emsp; TA (Track Angle): {commb.trk50(msg)}<br>&emsp;&emsp;&emsp;&emsp; GS (Ground Speed): {commb.gs50(msg)}<br><br>Item: I021/161 (Track Number)<br>&emsp;&emsp;&emsp;&emsp; TN (Track Number): NA<br><br>
					Item: I021/165 (Track Angle Rate)<br>&emsp;&emsp;&emsp;&emsp; TAR (Track Angle Rate): {commb.trk50(msg)}<br><br>Item: I021/230 (Roll Angle)<br>&emsp;&emsp;&emsp;&emsp; RA (Roll Angle): {commb.roll50(msg)}<br><br>
					Item: I021/295 (Data Ages)<br>&emsp;&emsp;&emsp;&emsp; DA(Data Age): {count}'''
					x[12]=df2x_50


				elif BDS == "BDS60":
					
					df2x_60=f'''Timestamp: {unix_epoch} {date_time}<br><br>Item: I021/080 (Target Address)<br>&emsp;&emsp;&emsp;&emsp; Addr (Target Address): {icao}<br><br>Item: I021/250 (Mode S MB Data)<br>&emsp;&emsp;&emsp;&emsp; MB (MB Data): {pms.data(msg)}<br><br>
					Item: I021/I150 (Air Speed)<br>&emsp;&emsp;&emsp;&emsp; IAS (Air Speed): {commb.ias60(msg)}<br><br>
					Item: I021/152 (Magnetic Heading)<br>&emsp;&emsp;&emsp;&emsp; MH (Magnetic Heading): {commb.hdg60(msg)}<br><br>Item: I021/155 (Barometric Vertical Rate)<br>&emsp;&emsp;&emsp;&emsp; BVR (Barometric Vertical Rate): {commb.vr60baro(msg)}<br><br>Item: I021/157 (Geometric Vertical Rate)<br>&emsp;&emsp;&emsp;&emsp; GVR (Geometric Vertical Rate): {commb.vr60ins(msg)}<br><br>
					Item: I021/295 (Data Ages)<br>&emsp;&emsp;&emsp;&emsp; DA(Data Age): {count}'''
					x[13]=df2x_60


				elif BDS == "BDS44":
					
					df2x_44=f'''Timestamp: {unix_epoch} {date_time}<br><br>Item: I021/080 (Target Address)<br>&emsp;&emsp;&emsp;&emsp; Addr (Target Address): {icao}<br><br>Item: I021/250 (Mode S MB Data)<br>&emsp;&emsp;&emsp;&emsp; MB (MB Data): {pms.data(msg)}<br><br>
					Item: I021/220 (MET Information)<br>&emsp;&emsp;&emsp;&emsp; WS (Wind Speed): {commb.wind44(msg)[0]}<br>&emsp;&emsp;&emsp;&emsp; 
        			WD (Wind Direction): {commb.wind44(msg)[1]}<br>&emsp;&emsp;&emsp;&emsp; Temperature 1: {commb.temp44(msg)[0]}<br>&emsp;&emsp;&emsp;&emsp; Temperature 2: {commb.temp44(msg)[1]}<br>&emsp;&emsp;&emsp;&emsp; Pressure: {commb.p44(msg)}<br>&emsp;&emsp;&emsp;&emsp; Humidity: {commb.hum44(msg)}<br>&emsp;&emsp;&emsp;&emsp; Turbulence: {commb.turb44(msg)}<br><br>
					Item: I021/295 (Data Ages)<br>&emsp;&emsp;&emsp;&emsp; DA(Data Age): {count}'''
					x[14]=df2x_44


				elif BDS == "BDS45":
				
					df2x_45=f'''Timestamp: {unix_epoch} {date_time}<br><br>Item: I021/080 (Target Address)<br>&emsp;&emsp;&emsp;&emsp; Addr (Target Address): {icao}<br><br>
					Item: I021/250 (Mode S MB Data)<br>&emsp;&emsp;&emsp;&emsp; MB (MB Data): {pms.data(msg)}<br><br>
					Item: I021/295 (Data Ages)<br>&emsp;&emsp;&emsp;&emsp; DA(Data Age): {count}'''
					x[15]=df2x_45

			count = 0

	else:
		print(count)
		count= count+1
		if count == 1800:
			count = 0
		else:
			pass
	return str(x)

#Store dat in Database
while True:
    try:
        r = redis.StrictRedis(host=redis_host, port=redis_port,decode_responses=True)
        r.set("airplaneData", data())
        msg = r.get("airplaneData")
        print("RedisDbData: ",msg,"\n")        
   
    except Exception as e:
        print(e)


 

	