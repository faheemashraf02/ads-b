# from socket import socket,AF_INET,SOCK_DGRAM
import socket
import struct
from sic_sac_sid_sms import cat23Data
from gpsData import GpsData
import pyModeS as pms
from datetime import datetime
from pyModeS import common, adsb, commb, bds
import asyncio
import websockets
import time
odd ='Odd'
even='Even'
x=[0]*14


s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('192.168.1.1', 10003))
mreq = struct.pack("=4sl", socket.inet_aton("232.1.1.11"), socket.INADDR_ANY)

def data():
	infoGps=GpsData()
	date_time =f'{infoGps[1]} {infoGps[3]} (UTC)'
	m=s.recv(10240)
	# file = open('/home/three_d/Desktop/def.txt', 'a')
	# file.write(str(m))
	# file.write('\n\n')
	
	msg = m[9:].hex()
	cat23=cat23Data()
	cat23=cat23.replace(",", ":")
	cat23=list(cat23.split(":")) 
	sid=f'( SID(Service Identification)= {cat23[31]}; STYP(Type of Service)= {cat23[33]} )'
	service_Management=f'( RP(Report Period for Category 021 Reports)= {cat23[41]}; SC(Service Class)= {cat23[43]} ; spare(Spare bit set to zero)= {cat23[45]}; FX(FX)= {cat23[47]} ; SSRP(Service Status Reporting Period)= {cat23[49]} )'
	
	if msg[0] == "8" or msg[0] == "a":
			df = common.df(msg)
			icao = common.icao(msg)
			message_bin = pms.hex2bin(msg)
			cap_field = pms.bin2int(message_bin[5:8])
			if df == 17 or df == 18:
				tc = common.typecode(msg)
			
				if 1 <= tc <= 4:  # callsign
					
					callsign = adsb.callsign(msg)
					ec=pms.adsb.category(msg)
					df17_1_4="Date & Time: "+str(date_time) +"$"+"Downlink Format: "+str(df) +"$"+"Type: Identitification_and_category"+"$"+"ICAO address: "+str(icao)+"$"+"Capability Field:"+str(cap_field) +"$"+"Callsign: "+str(callsign)+"$"+"Emitter Category:"+str(ec)
					
					x[0]=df17_1_4

				if 5 <= tc <= 8:  # surface position
					
					oe = adsb.oe_flag(msg)
					msgbin = common.hex2bin(msg)
					cprlat = common.bin2int(msgbin[54:71]) / 131072.0
					cprlon = common.bin2int(msgbin[71:88]) / 131072.0
					v = adsb.surface_velocity(msg)
					cord = pms.adsb.airborne_position_with_ref(msg, float(infoGps[6]) , float(infoGps[8]))
					lat, lon = cord[0], cord[1]
					df17_5_8= f'''Date & Time : {date_time}$ Message: {msg}$ Downlink Format: {df}$ Protocol: Mode-S Extended Squitter (ADS-B)$Type: Surface Position$ ICAO address: {icao}$ Capability Field: {cap_field}$ CPR format: { odd if oe else even}$ CPR Latitude: {cprlat}$ CPR Longitude: {cprlon}$ Speed: {v[0]} knots$ Track: {v[1]} degrees$ Latitude: {lat}$ Longitude: {lon} '''

					x[1]=df17_5_8

				if 9 <= tc <= 18:
				   
					alt = adsb.altitude(msg)
					oe = adsb.oe_flag(msg)
					msgbin = common.hex2bin(msg)
					nuc_p = tuple(map(str,pms.adsb.nuc_p(msg)))
					nuc_p='; '.join(map(str, nuc_p))
					cprlat = common.bin2int(msgbin[54:71]) / 131072.0
					cprlon = common.bin2int(msgbin[71:88]) / 131072.0
					cord = pms.adsb.airborne_position_with_ref(msg, float(infoGps[6]), float(infoGps[8]))
					lat, lon = cord[0], cord[1]
					df17_9_18= f'''Date & Time : {date_time}$ SAC: {cat23[21]}$ SIC: {cat23[23]}$ Downlink Format: {df}$ Protocol: Mode-S Extended Squitter (ADS-B)$ Type: Airborne position (with barometric altitude)$ ICAO address: {icao}$ Capability Field: {cap_field}$ Navigation Uncertainity Category Position(NUC_p): {nuc_p}$ CPR format:{ odd if oe else even}$ CPR Latitude: {cprlat}$ CPR Longitude: {cprlon}$ Altitude: {alt} feet$ Latitude: {lat}$ Longitude: {lon}$ SID: {sid} '''
					
					x[2]=df17_9_18


				if tc == 19:
					
					spd, trk, vr, t = adsb.velocity(msg)
					nuc_v = tuple(map(str,pms.adsb.nuc_v(msg)))
					nuc_v='; '.join(map(str, nuc_v))
					types = {"GS": "Ground speed", "TAS": "True airspeed"}
					df17_19= f'''Date & Time : {date_time}$ Message: {msg}$ Downlink Format: {df}$ Protocol: Mode-S Extended Squitter (ADS-B)$ Type: Airborne velocity$ ICAO address: {icao}$ Capability Field: {cap_field}$ Navigation Uncertainity Category Velocity(NUC_v): {nuc_v}$ Speed: {spd} knots$Track: {trk} degrees$ Vertical rate: {vr} feet/minute$ Type: {types[t]}$ Service Management: {service_Management} '''
					
					x[3]=df17_19
					

				if 20 <= tc <= 22:  # airborne position

					surv_status=pms.bin2int(message_bin[37:39])
					alt = adsb.altitude(msg)
					oe = adsb.oe_flag(msg)
					msgbin = common.hex2bin(msg)
					cprlat = common.bin2int(msgbin[54:71]) / 131072.0
					cprlon = common.bin2int(msgbin[71:88]) / 131072.0
					cord = pms.adsb.airborne_position_with_ref(msg, float(infoGps[6]), float(infoGps[8]))
					lat, lon = cord[0], cord[1]
					df17_20_22 = f'''Date & Time : {date_time}$ Message: {msg}$ Downlink Format: {df}$ Protocol: Mode-S Extended Squitter (ADS-B)$ Type: Airborne position (with GNSS altitude)$ ICAO address: {icao}$ Capability Field: {cap_field}$ CPR format: {odd if oe else even}$ CPR Latitude: {cprlat}$CPR Longitude: {cprlon}$ Altitude: {alt} feet$ Latitude: {lat}$ Longitude: {lon}$ Surveillance Status: {surv_status}'''
					
					x[4]=df17_20_22

				if tc == 28:
					sub_type = pms.bin2int(message_bin[37:40])
					if sub_type == 1:
						priority_status = pms.bin2int(message_bin[40:43]) #Priority Status
						mode_a_code = pms.bin2int(message_bin[43:56])

						#"Protocol : Mode-S Extended Squitter (ADS-B)"
						#"Type : 1090ES Aircraft Status Message (Emergency/Priority Status)"
						df17_28_1=f'''Date & Time : {date_time}$ Message: {msg}$ Downlink Format: {df}$ Protocol: Mode-S Extended Squitter (ADS-B)$ Type: 1090ES Aircraft Status Message (Emergency/Priority Status)$ ICAO address: {icao}$ Capability Field: {cap_field}$ Priority Status: {priority_status }$ Mode3/A code: {mode_a_code} '''
						x[5]=df17_28_1

					if sub_type == 2:
						ara = pms.bin2int(message_bin[40:54]) #Active Resolution Advisories(ARA)
						rac_records = pms.bin2int(message_bin[54:58]) #RACs Record
						rat = pms.bin2int(message_bin[58]) #RA Terminated
						mte = pms.bin2int(message_bin[59]) #Multiple Threat Encounter
						tti = pms.bin2int(message_bin[60:62]) #Threat Type Indicator
						tid = pms.bin2int(message_bin[62:88]) #Threat Identity Data
						#"Protocol : Mode-S Extended Squitter (ADS-B)"
						#"Type : 1090ES Aircraft Status Message (TCAS RA Broadcast Message)"
						df17_28_2=f'''Date & Time : {date_time}$ Message: {msg}$ Downlink Format: {df}$ Protocol: Mode-S Extended Squitter (ADS-B)$ Type: 1090ES Aircraft Status Message (TCAS RA Broadcast Message)$ ICAO address: {icao}$ Capability Field: {cap_field}$ Active Resolution Advisories(ARA): {ara}$ RACs Record: {rac_records}$RA Terminated: {rat}$ Multiple Threat Encounter: {mte}$ Threat Type Indicator: {tti}$ Threat Identity Data: {tid} '''
						x[5]=df17_28_2

				if tc == 29:
					sil_suplememt = pms.bin2int(message_bin[39]) #SIL Supplement
					selected_alt_type = pms.bin2int(message_bin[40]) #Selected Altitude Type
					MCP_FMS_Sel_Alt = pms.bin2int(message_bin[41:52]) #MCP/FCU Selected Altitude
					baro_pre_set = pms.bin2int(message_bin[52:61]) #Barometric Pressure Settings
					sel_head_stat = pms.bin2int(message_bin[61]) #Selected Heading Status
					sel_head_sign = pms.bin2int(message_bin[62])#Selected Heading Sign
					nac_p = pms.adsb.nac_p(msg) #Navigation Accuracy Category - Position
					nic_baro = pms.bin2int(message_bin[75]) #Navigation Integrity Category_Baro Level(NIC BARO)
					mcp_fcu_mode_bits = pms.bin2int(message_bin[78]) #MCP/FCU Mode Bits
					auto_pilot_eng = pms.bin2int(message_bin[79]) #AutoPilot Engaged
					vnav_mode_eng = pms.bin2int(message_bin[80]) #VNAV Mode Engaged
					alt_hold_mod = pms.bin2int(message_bin[81]) #Altitude Hold Mode
					app_mode = pms.bin2int(message_bin[83]) #Approach Mode
					tcas_op = pms.bin2int(message_bin[84]) #TCAS Operational
					sil =tuple(map(str,pms.adsb.sil(msg,2))) 
					sil='; '.join(map(str,sil))
					nac =tuple(map(str,pms.adsb.nac_p(msg)))
					nac='; '.join(map(str, nac))  
					df17_29 = f'''Date & Time: {date_time}$ Message: {msg}$ Downlink Format: {df}$ Protocol: Mode-S Extended Squitter (ADS-B)$Type: Target State and Status Information$ ICAO address: {icao}$ Capability Field: {cap_field}$ Surveillance integrity level(SIL): {sil}$ Navigation Accuracy Category(NAC): {nac}$SIL Supplement: {sil_suplememt}$ Selected Altitude Type: {selected_alt_type}$ MCP/FCU Selected Altitude: {MCP_FMS_Sel_Alt}$Barometric Pressure Settings: {baro_pre_set}$ Selected Heading Status: {sel_head_stat}$ Selected Heading Sign: {sel_head_sign}$ Navigation Accuracy Category Position: {nac_p}$ Navigation Integrity Category_Baro Level: {nic_baro}$ MCP/FCU Mode Bits: {mcp_fcu_mode_bits}$ AutoPilot Engaged: {auto_pilot_eng}$ VNAV Mode Engaged: {vnav_mode_eng}$ Altitude Hold Mode: {alt_hold_mod}$ Approach Mode: {app_mode}$ TCAS Operational: {tcas_op} '''
					
					x[6]=df17_29

				if tc == 31:
					sub_type = pms.bin2int(message_bin[37:40])
					if sub_type == 0:
						tcas_ops = pms.bin2int(message_bin[42]) #TCAS Operational: 
						es_cap_1090 = pms.bin2int(message_bin[43]) #Aircraft ADS-B 1090ES Receive Capability: 
						arv_cap = pms.bin2int(message_bin[46]) #Air-Referenced Velocity Report(ARV) Report Capability: 
						ts = pms.bin2int(message_bin[47]) #Target State Report Capability(TS): 
						tc = pms.bin2int(message_bin[48:50]) #Target Change Report Capability(TC): 
						nacv = pms.bin2int(message_bin[48:51]) #NACv: 
						nic_p = pms.bin2int(message_bin[51]) #NIC Supplement: 
						uat_rec_cap = pms.bin2int(message_bin[50]) #UAT Recieve Capability: 
						icas_ad = pms.bin2int(message_bin[58]) #TCAS Report Advisory Active: 
						indent_sw = pms.bin2int(message_bin[59]) #INDENT Switch Active: 
						rec_atc = pms.bin2int(message_bin[60]) 	#Reserved for Recieving ATC Services: 
						single_antenna_flag = pms.bin2int(message_bin[61]) #Single Antenna Flag: 
						sys_design_as = pms.bin2int(message_bin[62:64]) #System Design Assurance: 
						adsb_v = pms.bin2int(message_bin[72:75]) #MOPS Version: 
						gva = pms.bin2int(message_bin[80:82]) #Geometric Vertical Accuracy (GVA): 
						sil = pms.bin2int(message_bin[82:84]) #Source Integity Level(SIL): 
						nic_baro = pms.bin2int(message_bin[84]) #NICbaro :
						hrd = pms.bin2int(message_bin[85]) #HRD :
						sil_s = pms.bin2int(message_bin[86]) #SIL Suppl :
						"Protocol : Mode-S Extended Squitter (ADS-B)"
						"Type : Aircraft Operational Status(Airborne)"
						df17_31_0 = f'''Date & Time : {date_time}$ Message: {msg}$Downlink Format: {df}$ Protocol: Mode-S Extended Squitter (ADS-B)$Type: Aircraft Operation Status$ ICAO address: {icao}$Capability Field: {cap_field}$ ADS-B version number: {adsb_v}$Surveillance integrity level(SIL): {sil}$ TCAS Operational: {tcas_ops}$Aircraft ADS-B 1090ES Receive Capability: {es_cap_1090}$ Air-Referenced Velocity Report Capability: {arv_cap}$Target Change Report Capability(TC): {tc}$Target State Report Capability(TS): {ts}$NACv: {nacv}$ NIC Supplement: {nic_p}$ UAT Recieve Capability: {uat_rec_cap}$TCAS Report Advisory Active: {icas_ad}$ INDENT Switch Active: {indent_sw}$Reserved for Recieving ATC Services: {rec_atc}$ Single Antenna Flag: {single_antenna_flag}$System Design Assurance: {sys_design_as}$ MOPS Version: {adsb_v}$ Geometric Vertical Accuracy: {gva}$Source Integity Level: {sil}$ NICbaro: {nic_baro}$ HRD: {hrd}$ SIL Suppl: {sil_s}'''
						x[7]=df17_31_0

					if sub_type == 1:
						lon_gps_antenna=0
						dir_n=0
						heigh_t=0

						poa = pms.bin2int(message_bin[42]) #Position Offset Applied(POA): 
						es_cap_1090 = pms.bin2int(message_bin[43]) #Aircraft ADS-B 1090ES Receive Capability:
						b2_low = pms.bin2int(message_bin[46]) # b2 Low
						uat_rec_cap = pms.bin2int(message_bin[47]) #UAT Receive Capability: 
						nacv = pms.bin2int(message_bin[48:51]) #NACv: 
						nic_sup = pms.bin2int(message_bin[51]) #NIC Supplement: 
						l_w = pms.bin2int(message_bin[52:56]) #Length/Width of the Aircraft:
						tcas_rep_ad = pms.bin2int(message_bin[58]) #TCAS Report Advisory Active: 
						indent_sw = pms.bin2int(message_bin[59]) #INDENT Switch Active: 
						rec_atc = pms.bin2int(message_bin[60]) #Reserved for Recieving ATC Services:  
						single_antenna_flag = pms.bin2int(message_bin[61]) #Single Antenna Flag:  
						sys_des_as = pms.bin2int(message_bin[62:64]) #System Design Assurance:  
						adsb_v = pms.bin2int(message_bin[72:75]) #MOPS Version: 
						gps_an_of = message_bin[64:67]
						if gps_an_of[0] == '0':
						    dir_n="Left"
						elif gps_an_of[0] == '1':
						    dir_n="Right"
						if gps_an_of[1:3] == '00':
						    heigh_t='0 m'
						elif gps_an_of[1:3] == '01':
						    heigh_t='2 m'
						elif gps_an_of[1:3] == '10':
						    heigh_t='4 m'
						elif gps_an_of[1:3] == '11':
						    heigh_t='6 m'    
						buff="("+dir_n+";"+heigh_t+")"	
						
						i = pms.bin2int(message_bin[67:72])
						if i in range(1,32):
							lon_gps_antenna = str(i+i)+" "+"m" 
						elif i == 0:
						    lon_gps_antenna = 0
						sil = pms.adsb.sil(msg,2) #Source Integity Level(SIL):
						trk_hdg = pms.bin2int(message_bin[84]) #TRK/HDG :
						hrd = pms.bin2int(message_bin[85]) #HRD :
						sil_supl = pms.bin2int(message_bin[86]) #SIL Suppl
						
						
						df17_31_1=f'''Date & Time : {date_time}$ Message: {msg}$ Downlink Format: {df}$ Protocol: Mode-S Extended Squitter (ADS-B)$Type: Aircraft Operational Status(Surface)$ ICAO address: {icao}$ Capability Field: {cap_field}$ Position Offset Applied(POA): {poa}$ Aircraft ADS-B 1090ES Receive Capability: {es_cap_1090}$b2 Low: {b2_low}$ UAT Receive Capability: {uat_rec_cap}$ NACv: {nacv}$ NIC Supplement: {nic_sup}$ Length/Width of the Aircraft: {l_w}$TCAS Report Advisory Active: {tcas_rep_ad}$ INDENT Switch Active: {indent_sw}$Reserved for Recieving ATC Services: {rec_atc}$ Single Antenna Flag: {single_antenna_flag}$System Design Assurance: {sys_des_as}$ MOPS Version: {adsb_v}$ Lateral GPS Antenna Offset:{buff}$ Longitudinal GPS Antenna Offset: {lon_gps_antenna}$Source Integity Level(SIL): {sil}$ TRK/HDG: {trk_hdg}$ HRD: {hrd}$ SIL Suppl: {sil_supl}'''
						x[7]=df17_31_1	

							
		
			alt = 0
			sqw = 0


			if df == 20:
				alt = str(common.altcode(msg))

			if df == 21:
				sqw = common.idcode(msg)

			if df == 20 or df == 21:
				labels = {
					"BDS10": "Data Link Capability",
					"BDS17": "GICB Capability",
					"BDS20": "Aircraft Identification",
					"BDS30": "ACAS Resolution",
					"BDS40": "Vertical Intention Report",
					"BDS50": "Track and Turn Report",
					"BDS60": "Heading and Speed Report",
					"BDS44": "Meteorological Routine Air Report",
					"BDS45": "Meteorological Hazard Report",
					"EMPTY": "[No information available]",
				}

				BDS = bds.infer(msg, mrar=True)

				if BDS == "BDS20":
					callsign = commb.cs20(msg)
					
					df2x_20 = f'''Date & Time: {date_time}$ Message: {msg}$ Downlink Format: {df}$ Protocol: Mode-S Comm-B identity reply$ Report Type: {labels[BDS]}$ ICAO address: {icao}$ Altitude: {alt} feet$ Squawk code: {sqw}$ Callsign: {callsign} '''
					
					x[8]=df2x_20


				if BDS == "BDS40":
					
					msg_data = pms.data(msg)
					msg_bin = pms.hex2bin(msg_data)
					app_mode = pms.bin2int(msg_bin[13]) # Approach Mode
					alt_hold_mode = pms.bin2int(msg_bin[14]) # Altitude Hold Mode
					man_vert__mode = pms.bin2int(msg_bin[15]) # Manage Vertical Mode
					df2x_40 = f'''Date & Time: {date_time}$ Message: {msg}$ Downlink Format: {df}$ Protocol: Mode-S Comm-B identity reply$ Report Type: {labels[BDS]}$ ICAO address: {icao}$ Altitude: {alt} feet$ Squawk code: {sqw}$ MCP target alt: {commb.selalt40mcp(msg)} feet$FMS Target alt: {commb.selalt40fms(msg)} feet$ Pressure: {commb.p40baro(msg)} millibar$ Approach Mode: {app_mode}$ Altitude Hold Mode: {alt_hold_mode}$ Manage Vertical Mode: {man_vert__mode} '''
					
					x[9]=df2x_40

				if BDS == "BDS50":
					
					df2x_50 = f'''Date & Time: {date_time}$ Message: {msg}$ Downlink Format: {df}$ Protocol: Mode-S Comm-B identity reply$Report Type: {labels[BDS]}$ ICAO address: {icao}$ Altitude: {alt} feet$ Squawk code: {sqw}$ Track angle: {commb.trk50(msg)}degrees$ Roll angle: {commb.roll50(msg)} degrees$ Track rate: {commb.rtrk50(msg)} degree/second$ Ground speed: {commb.gs50(msg)} knots$True airspeed: {commb.tas50(msg)} knots '''
					
					x[10]=df2x_50


				if BDS == "BDS60":
					
					df2x_60 = f'''Date & Time: {date_time}$ Message: {msg}$ Downlink Format: {df}$ Protocol: Mode-S Comm-B identity reply$Report Type: {labels[BDS]}$ ICAO address: {icao}$ Altitude: {alt} feet$ Squawk code: {sqw}$ Magnetic Heading: {commb.hdg60(msg)} degrees$Indicated airspeed: {commb.ias60(msg)} knots$ Mach number: {commb.mach60(msg)}$ Vertical rate (Baro): {commb.vr60baro(msg)} feet/minute$Vertical rate (INS): {commb.vr60ins(msg)} feet/minute '''
					
					x[11]=df2x_60


				if BDS == "BDS44":
					
					df2x_44 = f'''Date & Time: {date_time}$ Message: {msg}$ Downlink Format: {df}$ Protocol: Mode-S Comm-B identity reply$ Report Type: {labels[BDS]}$ICAO address: {icao}$ Altitude: {alt} feet$ Squawk code: {sqw}$ Wind speed: {commb.wind44(msg)[0]} knots$ Wind direction: {commb.wind44(msg)[1]} degrees$Temperature 1: {commb.temp44(msg)[0]} celsius$ Temperature 2: {commb.temp44(msg)[1]} celsius$ Pressure: {commb.p44(msg)} hPa$Humidity: {commb.hum44(msg)} %$ Turbulence: {commb.turb44(msg)} '''
					
					x[12]=df2x_44


				if BDS == "BDS45":
				
					df2x_45 = f'''Date & Time: {date_time}$ Message: {msg}$ Downlink Format: {df}$ Protocol: Mode-S Comm-B identity reply$ Report Type: {labels[BDS]}$ICAO address: {icao}$ Altitude: {alt} feet$ Squawk code: {sqw}$ Turbulence: {commb.turb45(msg)}$ Wind shear: {commb.ws45(msg)}$Microbust: {commb.mb45(msg)}$ Icing: {commb.ic45(msg)}$ Wake vortex: {commb.wv45(msg)}$ Temperature: {commb.temp45(msg)} celsius$Pressure: {commb.p45(msg)} hPa$ Radio height: {commb.rh45(msg)} feet  '''
					
					x[13]=df2x_45
	
	return str(x)
# while True:
 
#  print(data())
#  print("\n")
 

	
