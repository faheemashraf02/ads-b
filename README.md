## **ADS-B**

ADS-B is short for Automatic Dependent Surveillance–Broadcast. It is a satellite based surveillance system. Aircraft position, velocity, together with 
identification are transmitted through Mode-S Extended Squitter (1090 MHz).

Majority of the aircraft nowadays are broadcasting ADS-B messages constantly. There are many ways you can set up you own receiver and antenna to start
tapping into those signals (DVB-T USB stick, Mode-S Beast, Raspberry Pi, RadarScape, etc).

### ADS-B Basics

#### Message structure

An ADS-B message is 112 bits long, and consists of 5 parts.

+--------+--------+-----------+--------------------------+---------+
|  DF 5  |  ** 3  |  ICAO 24  |          DATA 56         |  PI 24  |
+--------+--------+-----------+--------------------------+---------+
Any ADS-B must start with the Downlink Format 17, or 18 in case of TIS-B message. They correspond to 10001 or 10010 in binary for the first 5 bits. Bits 6-8 are used as an additional identifier, which has different meanings within each ADS-B subtype.

In following Table [tb:adsb-structure], the key information of a ADS-B message is listed.

nBits	Bits	Abbr.	Name
5	1 - 5	DF	Downlink Format
3	6 - 8	CA	Capability (additional identifier)
24	9 - 32	ICAO	ICAO aircraft address
56	33 - 88	DATA	Data
[33 - 37]	[TC]	Type code
24	89 - 112	PI	Parity/Interrogator ID

An example:

Raw message in hexadecimal:
8D4840D6202CC371C32CE0576098

[00100]0000010110011
00001101110001110000
110010110011100000

-----+------------+--------------+----------------------+--------------
HEX  | 8D         | 4840D6       | 202CC371C32CE0       | 576098
-----+------------+--------------+----------------------+--------------
BIN  | 10001  101 | 010010000100 | [00100]0000010110011 | 010101110110
     |            | 000011010110 | 00001101110001110000 | 000010011000
     |            |              | 110010110011100000   |
-----+------------+--------------+----------------------+--------------
DEC  |  17    5   |              | [4] ...............  |
-----+------------+--------------+----------------------+--------------
     |  DF    CA  |   ICAO       | [TC] --- DATA -----  | PI
     
ADS-B message types
To identify what information is contained in an ADS-B message, we need to take a look at the Type Code of the message, indicated at bits 33 - 37 of the ADS-B message (or first 5 bits of the DATA segment).

In following Table [tb:adsb-tc], the relationships between each Type Code and its information contained in the DATA segment are shown.

ADS-B Type Code and content

#### Type Code	Content

1 - 4	Aircraft identification
5 - 8	Surface position
9 - 18	Airborne position (w/ Baro Altitude)
19	Airborne velocities
20 - 22	Airborne position (w/ GNSS Height)
23 - 27	Reserved
28	Aircraft status
29	Target state and status information
31	Aircraft operation status     
     

### Enhanced Mode-S Basics

#### Downlink Format and message structure

DF 20 and DF 21 are used for downlink messages.

The same as ADS-B, in all Mode-S messages, the first 5 bits contain the Downlink Format. The same identification process can be used to discover EHS messages. So the EHS messages starting bits are:

DF20 - 10100
DF21 - 10101
The message is structured as follows, where the digit represents the number of binary digits:

+--------+--------+--------+--------+------------+---------+------------+
|  DF 5  |  FS 3  |  DR 5  |  UM 6  |  AC/ID 13  |  MB 56  |  AP/DP 24  |
+-----------------+--------+--------+------------+---------+------------+
|   <---------+        32 bits       +-------->  |


DF:     downlink format
FS:     flight status
DR:     downlink request
UM:     utility message
AC/ID:  altitude code (DF20) or identity (DF21)
MB:     message, Comm-B
AP/DP:  address parity or data parity
Except for the DF, the first 32 bits do not contain useful information to decode the message. The exact definitions can be found in ICAO annex 10 (Aeronautical Telecommunications).     